#ifndef WORLD_HPP_
#define WORLD_HPP_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "Config.hpp"
#include "Time.hpp"

class LWorld{
	public:
		LWorld(LConfig *config, LTime *time, char *name, int width, int height);
		~LWorld();
		
		void update(SDL_Event *event);
		void render(SDL_Renderer *renderer);
	private:
		LConfig *_config;
		LTime *_time;
		
		char *_name;
		
		int _width;
		int _height;
};

#endif // WORLD_HPP_