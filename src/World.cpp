#include "World.hpp"

LWorld::LWorld(LConfig *config, LTime *time, char *name, int width, int height){
	_config = config;
	_time = time;
	_name = name;
	_width = width;
	_height = height;
}

LWorld::~LWorld(){
	delete _config;
	delete _time;
}

void LWorld::update(SDL_Event *event){
	_time->update();
	std::cout << _time->getSecond() << "\n";
}

void LWorld::render(SDL_Renderer *renderer){
	SDL_SetRenderDrawColor(renderer, 0, 0, _time->getSecond() * 10, 255);
}