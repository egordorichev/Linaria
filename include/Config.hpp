#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#include <libconfig.h++>
#include <iostream>
#include <string>

class LConfig{
	public:	
		LConfig(const char *file);
		~LConfig();
		
		bool find(const char *variable);
		
		char *readChar(const char *variable);
		int readInt(const char *variable);
		float readFloat(const char *variable);
		bool readBool(const char *variable);
		
		void writeChar(const char *variable, const char *value);
		void writeInt(const char *variable, int value);
		void writeFloat(const char *variable, float value);
		void writeBool(const char *variable, bool value);
	private:	
		libconfig::Config _config;
		
		char *_file;
};

#endif // CONFIG_HPP_