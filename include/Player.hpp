#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "Config.hpp"

class LPlayer{
	public:
		LPlayer(LConfig *config, int id, char* name, int spawnX, int spawnY);
		~LPlayer();
		
		void update(SDL_Event *event);
		void render(SDL_Renderer *renderer);
	private:
		LConfig *_config;
	
		int _id;
		int _spawnX;
		int _spawnY;
		
		char *_name;
};

#endif // PLAYER_HPP_