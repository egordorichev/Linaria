#include "Time.hpp"

LTime::LTime(int hour, int minute, int second){
	_hour = hour;
	_minute = minute;
	_second = second;
	_lastUpdate = SDL_GetTicks();
}

void LTime::update(){
	if(SDL_GetTicks() - _lastUpdate > 1000){
		_lastUpdate += 1000;
		_second++;
		
		if(_second >= 61){
			_second -= 60;
			_minute++;
			
			if(_minute >= 61){
				_minute -= 60;
				_hour++;
				
				if(_hour >= 25){
					_hour -= 24;
				}
			}
		}
	}
}