#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include <iostream>

#include "World.hpp"
#include "Time.hpp"
#include "Player.hpp"
#include "Config.hpp"

#define FPS 60

SDL_Window *Window;
SDL_Renderer *Renderer;
SDL_Event Event;

LPlayer *Player;
LWorld *World;
LConfig *WorldConfig;
LConfig *PlayerConfig;

bool running = true;

int lastFrameTicks;
int currentFrameTicks;

int main(){
	SDL_Init(SDL_INIT_EVERYTHING); // Init SDL
	
	Window = SDL_CreateWindow("Linaria v.0.1 develop", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE); // Create window
	
	if(!Window){ // If here was an error creating window
		std::cout << SDL_GetError() << "\n";
		
		return -1;
	}
	
	Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED); // Create renderer
	
	if(!Renderer){ // If here was an error creating renderer
		std::cout << SDL_GetError() << "\n";
		
		return -2;
	}
	
	WorldConfig = new LConfig(".config/world.conf"); // Create World Config
	World = new LWorld(WorldConfig, new LTime(WorldConfig->readInt("world.time.hour"), WorldConfig->readInt("world.time.minute"), WorldConfig->readInt("world.time.second")), WorldConfig->readChar("world.name"), WorldConfig->readInt("world.width"), WorldConfig->readInt("world.height")); // Hello, World! 
	PlayerConfig = new LConfig(".config/player.conf"); // Create Player Config
	Player = new LPlayer(PlayerConfig, PlayerConfig->readInt("player.id"), PlayerConfig->readChar("player.name"), PlayerConfig->readInt("player.spawnX"), PlayerConfig->readInt("player.spawnY")); // Create Player
	
	currentFrameTicks = SDL_GetTicks();
	
	while(running){
		lastFrameTicks = currentFrameTicks;
		currentFrameTicks = SDL_GetTicks();
		
		if(SDL_PollEvent(&Event)){
			switch(Event.type){
				case SDL_QUIT: running = false; break; // User tries to close window, so we are exiting
			}
		}
		
		// Update all
		
		World->update(&Event);
		Player->update(&Event);
		
		// Clear the renderer
		
		SDL_RenderClear(Renderer);
		
		// Draw evrething
		
		World->render(Renderer);
		Player->render(Renderer);
		
		// Render all  
		   	
	   	SDL_RenderPresent(Renderer);
		   
		// Limit FPS
		
		int currentFPS = SDL_GetTicks() - currentFrameTicks;
		
		if(1000 / FPS > currentFPS){
			SDL_Delay(1000 / FPS - currentFPS);
		}
	}
	
	// Cleanup
	
	SDL_DestroyRenderer(Renderer);
	SDL_DestroyWindow(Window); 
	SDL_Quit();
	
	return 0;
}