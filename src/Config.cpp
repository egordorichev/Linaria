#include "Config.hpp"

LConfig::LConfig(const char *file){
	_file = (char *)file;
	
	try{
		_config.readFile(_file); // Trying to open config 
	} catch(const libconfig::FileIOException &error){ // We can't open config
		std::cout << "Could not open config file " << _file << "\n";
	} catch(const libconfig::ParseException &error){ // It's a syntax error
		std::cout << "Syntax error in " << _file << " at line " << error.getLine() << " (" << error.getError() << ")\n";
	}
}

LConfig::~LConfig(){
	// Nothing to do there 8-)
}

bool LConfig::find(const char *variable){	
	try{
		_config.lookup(variable); // If we can find this variable in config
		
		return true;
	} catch(const libconfig::SettingNotFoundException &error){ // Opps... We did not find it
		std::cout << "Could not find variable " << variable << "\n";
		
		return false;
	}
}

char *LConfig::readChar(const char *variable){
	char *value;
	
	try{
		_config.lookupValue(variable, (const char *&)value); // If we can find this variable in config and read it
	} catch(const libconfig::SettingNotFoundException &error){ // Opps... We can't do that
		std::cout << "Could not find variable " << variable << "\n";
	}
	
	return value;
}

int LConfig::readInt(const char *variable){
	int value;
	
	try{
		_config.lookupValue(variable, value); // If we can find this variable in config and read it
	} catch(const libconfig::SettingNotFoundException &error){ // Opps... We can't do that
		std::cout << "Could not find variable " << variable << "\n";
	}
	
	return value;	
}

float LConfig::readFloat(const char *variable){
	float value;
	
	try{
		_config.lookupValue(variable, value); // If we can find this variable in config and read it
	} catch(const libconfig::SettingNotFoundException &error){ // Opps... We can't do that
		std::cout << "Could not find variable " << variable << "\n";
	}
	
	return value;	
}

bool LConfig::readBool(const char *variable){
	bool value;
	
	try{
		_config.lookupValue(variable, value); // If we can find this variable in config and read it
	} catch(const libconfig::SettingNotFoundException &error){ // Opps... We can't do that
		std::cout << "Could not find variable " << variable << "\n";
	}
	
	return value;	
}

void LConfig::writeChar(const char *variable, const char *value){
	_config.lookup(variable) = value; // Set the variable
	_config.writeFile(_file); // And write it to file
}

void LConfig::writeInt(const char *variable, int value){
	_config.lookup(variable) = value; // Set the variable
	_config.writeFile(_file); // And write it to file
}

void LConfig::writeFloat(const char *variable, float value){
	_config.lookup(variable) = value; // Set the variable
	_config.writeFile(_file); // And write it to file
}

void LConfig::writeBool(const char *variable, bool value){
	_config.lookup(variable) = value; // Set the variable
	_config.writeFile(_file); // And write it to file
}