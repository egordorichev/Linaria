#include "Player.hpp"

LPlayer::LPlayer(LConfig *config, int id, char* name, int spawnX, int spawnY){
	_config = config;
	_id = id;
	_name = name;
	_spawnX = spawnX;
	_spawnY = spawnY;
}

LPlayer::~LPlayer(){
	delete _config;
}

void LPlayer::update(SDL_Event *event){
	
}

void LPlayer::render(SDL_Renderer *renderer){
	
}