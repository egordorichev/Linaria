#ifndef TIME_HPP_
#define TIME_HPP_

#include <SDL2/SDL.h>

class LTime{
	public:
		LTime(int hour, int minute, int second);
		
		void update();
		
		int getHour(){ return _hour; };
		int getMinute(){ return _minute; };
		int getSecond(){ return _second; };
	private:
		int _lastUpdate;
		int _hour;
		int _minute;
		int _second;
};

#endif // TIME_HPP_